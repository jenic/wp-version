# WP-VERSION

_Determine the version of wordpress running on a website_

**Required Modules:**

* argparse
* hashlib
* time
* urllib2
* json

## Usage

    wp-version.py [-h] [--quick] [--update] [--verbose] [--wait N] addr
    
    Determine wordpress version of a website based on official api hashes of
    static content
    
    positional arguments:
      addr           an address with full path to WP: https://blog.example.com
    
    optional arguments:
      -h, --help     show this help message and exit
      --quick        Stop after first match
      --update       Update the wp_version_data.db
      --verbose      Print everything to stdout
      --wait N       Wait n seconds between requests
      
