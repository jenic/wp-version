#!/usr/bin/python2

import urllib2, json, sqlite3, re

DB_NAME = 'wp_version_data.db'
LT_V = 3.50
VERSIONS_URL = 'https://codex.wordpress.org/WordPress_Versions'
FILEDATA_URL = 'https://api.wordpress.org/core/checksums/1.0/?version=%s&locale=en_US'
CREATE_TBL = '''
            CREATE TABLE IF NOT EXISTS
            file_data(id INTEGER PRIMARY KEY,
            name TEXT,
            hash TEXT,
            version TEXT)
            '''
SELECT_Q = 'SELECT * FROM file_data WHERE name=? AND hash=?'
INSERT_Q = 'INSERT INTO file_data(name, hash, version) VALUES(?,?,?)'
DELETE_Q = 'DELETE FROM file_data WHERE name=? AND hash=?'

def vfloat(version):
    s = version.split('.')
    v = s[0]
    b = ''.join(s[1:])
    return float('.'.join((v,b)))

def sqlConnect(db):
    sql = sqlite3.connect(db)
    cur = sql.cursor()
    return (sql, cur)

def getStats(sqlHandle):
    LATEST_Q = 'SELECT version FROM file_data ORDER BY version DESC LIMIT 1'
    UNIQ_Q = 'SELECT count(DISTINCT version) FROM file_data'
    COUNT_Q = 'SELECT count(*) AS num FROM file_data'
    cur = sqlHandle.cursor()
    a = cur.execute(UNIQ_Q).fetchone()[0]
    b = cur.execute(COUNT_Q).fetchone()[0]
    c = cur.execute(LATEST_Q).fetchone()[0]
    return {'unique': a, 'total': b, 'latest': c}

def getVersions():    
    return re.findall(r'<b><a href="/V.+title="V.+">(.+)</a>',
            urllib2.urlopen(VERSIONS_URL).read())

def getFileData(args={}):
    hash_dict = {}
    unique_files = []
    allVersions = getVersions()

    if args.verbose:
        print "Downloading hashes from %i different versions\n"\
                % (len(allVersions))

    for version in allVersions:
        if vfloat(version) < LT_V:
            if args.verbose: print "Skipping %s < %s" % (version, LT_V)
            continue

        j = json.loads(urllib2.urlopen(FILEDATA_URL % version).read())
        checksums = j['checksums']
        if checksums:
            if args.verbose:
                print "%s has %i total checksums" % (version, len(checksums))

            for k in checksums:
                if '.php' not in k and 'wp-admin' not in k:
                    if not checksums[k] in hash_dict:
                        hash_dict[checksums[k]] = {
                                'filename': k,
                                'version': version,
                                'count': 1
                                }
                    else:
                        hash_dict[checksums[k]]['count'] += 1

        else:
            if args.verbose:
                print "%s did not contain checksums!" % (version)
        
    for hash in hash_dict:
        if hash_dict[hash]['count'] == 1:
            unique_files.append({
                'name': hash_dict[hash]['filename'], 
                'hash': hash,
                'version': hash_dict[hash]['version']
            })
           
    return unique_files

def main(args={}):
    sql, cur = sqlConnect(DB_NAME)
    cur.execute(CREATE_TBL)
    sql.commit()

    if args.verbose: print "Getting file data...\n"
    file_data = getFileData(args)

    if args.verbose: print "Putting file data into sql structure...\n"
    for file in file_data:
        name = file['name']
        hash = file['hash']
        version = file['version']
        
        cur.execute(SELECT_Q, (name, hash))
        if not cur.fetchone():
            cur.execute(INSERT_Q, (name, hash, version))
            sql.commit()
            if args.verbose: print "%s [v%s] Added.\n" % (name, version)
        else:
            cur.execute(DELETE_Q, (name, hash))
            
if __name__ == '__main__':
    main()
