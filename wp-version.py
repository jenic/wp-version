#!/usr/bin/python2
import argparse, sqlite3, hashlib, sys, urllib2, re
from time import sleep
from updatedb import sqlConnect, getStats, getVersions, vfloat

# Globals, too lazy to make a class structure
sql = cur = args = False

def sqlLookup(query):
    global sql, cur, args
    if args is False:
        # This shit is exactly why I hate python
        # http://stackoverflow.com/questions/2052390/manually-raising-throwing-an-exception-in-python
        raise ValueError("Cannot execute w/o parser")

    if sql or cur is False:
        sql, cur = sqlConnect(args.db)

    return cur.execute(query).fetchall()

def getFileList():
    files = sqlLookup('SELECT name FROM file_data')
    unique_files = []
    for file in files:
        if not file[0] in unique_files:
            unique_files.append(file[0])
    return sorted(unique_files)

def checkHash(digest):
    result = sqlLookup('SELECT * FROM file_data WHERE hash="%s"' % digest)
    if len(result) > 0:
        return result[0]

    return False

def main():
    # A class would solve this issue but again, too lazy
    global args, sql, cur
    inaccurate = False

    parser = argparse.ArgumentParser(description='''
    Determine wordpress version of a URL based on official API hashes of static
    content''')
    parser.add_argument('url', metavar='url', type=str,
            help='A url with full path to WP: https://blog.example.com')
    parser.add_argument('--quick', action='store_true', default=False,
            help='Stop after first match')
    parser.add_argument('--update', action='store_true', default=False,
            help='Update the wp_version_data.db')
    parser.add_argument('--verbose', action='store_true', default=False,
            help='Print everything to stdout')
    parser.add_argument('--wait', metavar='N', type=int,
            help='Wait n seconds between requests')
    parser.add_argument('--db', metavar='F', type=str,
            default='wp_version_data.db', help='path/to/sqlite3.db')
    parser.add_argument('--no-meta', action='store_true', default=False,
            help='Skip meta tag check')
    args = parser.parse_args()

    if args.verbose:
        for k, v in vars(args).items():
            print "{:<8} : {!s:<15}".format(k.title(), v)

    if args.update:
        import updatedb
        updatedb.main(args)

    if sql or cur is False:
        sql, cur = sqlConnect(args.db)

    stats = getStats(sql)
    print "Have %s distinct versions in %s total records up to v%s" %\
            (stats['unique'], stats['total'], stats['latest'])

    # Sanity Check
    latest = getVersions()[-1]
    if vfloat(latest) > vfloat(stats['latest']):
        print """WARNING: %s is most current version but database can only
         uniquely identify up to %s""" % (latest, stats['latest'])
        inaccurate = True

    # List of all file versions found. Greatest number in the list will be
    # returned as the wordpress version
    file_versions = []
    declared = None

    if args.no_meta is False:
        m = re.search(r'<meta.*generator.*WordPress\s([0-9.]+)"',
                urllib2.urlopen(args.url + '/').read())
        if m is None and args.verbose:
            print "Meta check failed, not WordPress?"
        else:
            declared = m.group(1)
            file_versions.append(declared)
            if args.verbose:
                print "Meta tag declares version as %s" % (declared)

    files = getFileList()
    for file in files:
        if declared is not None and inaccurate is True:
            print "STOPPING: Declared version %s is > detectable version %s" %\
                    (declared, stats['latest'])
            break

        if files[0] == file:
            output = '\nChecking "%s"...' % file
        else:
            output = 'Checking "%s"...' % file
        if args.verbose: print output

        try:
            r = urllib2.urlopen(args.url + '/' + file).read()
            rec = checkHash(hashlib.md5(r.encode('utf-8')).hexdigest())
        except KeyboardInterrupt:
            break
        except:
            rec = False

        if rec is not False:
            if args.verbose:
                print '\tVersion: %s \n\tFileName: %s\n\tHash: %s' % (
                        rec[3], rec[1], rec[2])

            if not rec[3] in file_versions:
                file_versions.append(rec[3])

            if args.quick:
                break
        else:
            pass

        if args.wait is not None:
            sleep(int(args.wait))

    if len(file_versions) > 0:
        print '\nDetermined Version: %s' % max(file_versions)
    else:
        print '\nCould not determine version'

if __name__ == "__main__":
    main()
